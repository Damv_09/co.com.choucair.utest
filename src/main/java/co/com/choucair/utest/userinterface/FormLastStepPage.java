package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class FormLastStepPage {
    public static final Target INPUT_PASSWORD = Target.the("where do we write a password ").
            located(By.id("password"));
    public static final Target INPUT_CONFIRMPASSWORD = Target.the("where do we write a confirm password ").
            located(By.id("confirmPassword"));
    public static final Target CHECK_ACCEPT_UTEST = Target.the("where do we select a checkbox for accept term utest ").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target CHECK_ACCEPT_PRIVACY = Target.the("where do we select a checkbox for accept privacy & security Policy").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));
    public static final Target COMPLETE_BUTTON = Target.the("button of register ").
            located(By.xpath("//*[@id=\"laddaBtn\"]"));
}
