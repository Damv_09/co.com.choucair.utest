package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class JoinTodayPage {
    public static final Target JOIN_BUTTON = Target.the("button that shows us the required forms ").
            located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a"));
    //Form Personal information
    public static final Target INPUT_FIRSTNAME = Target.the("where do we write the First name ").
            located(By.id("firstName"));
    public static final Target INPUT_LASTNAME = Target.the("where do we write the Last name ").
            located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email ").
            located(By.id("email"));
    public static final Target INPUT_DATEMONTH = Target.the("where do we select the  birth month ").
            located(By.id("birthMonth"));
    public static final Target INPUT_DATEDAY = Target.the("where do we select the  birth day ").
            located(By.id("birthDay"));
    public static final Target INPUT_DATEYEAR = Target.the("where do we select the  birth year ").
            located(By.id("birthYear"));
    public static final Target NEXTLOCATION_BUTTON = Target.the("button that shows us the form of Location ").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a"));



}
