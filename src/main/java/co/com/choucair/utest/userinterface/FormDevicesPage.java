package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class FormDevicesPage {
    public static final Target INPUT_COMPUTER = Target.the("Where do we select the computer").
            located(By.xpath("//*[@id=\"web-device\"]/div[1]/div[2]/div"));
    public static final Target SELECT_COMPUTER = Target.the("Bring the computer selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-3-1\"]/span/div"));
    public static final Target INPUT_VERSION = Target.the("Where do we select the version of computer").
            located(By.xpath("//*[@id=\"web-device\"]/div[2]/div[2]/div/div[1]"));
    public static final Target SELECT_VERSION = Target.the("Bring the version of computer selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-4-17\"]/span/div"));
    public static final Target INPUT_LANGUAGE = Target.the("Where do we select the lenguage").
            located(By.xpath("//*[@id=\"web-device\"]/div[3]/div[2]/div/div[1]"));
    public static final Target SELECT_LANGUAGE = Target.the("Bring the lenguage selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-5-37\"]/span/div"));
    public static final Target INPUT_MOBILEDEVICE = Target.the("Where do we select the movile device").
            located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div/div[1]"));
    public static final Target SELECT_MOBILEDEVICE = Target.the("Bring the movile device selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-23-12\"]"));
    public static final Target INPUT_MODEL = Target.the("Where do we select the model").
            located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]"));
    public static final Target SELECT_MODEL = Target.the("Bring the model selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-13-95\"]/span/div"));
    public static final Target INPUT_OS = Target.the("Where do we select the operating system").
            located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div"));
    public static final Target SELECT_OS = Target.the("Bring the operating system selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-14-8\"]/span/div"));
    public static final Target NEXTLASTSTEP_BUTTON = Target.the("button that shows us the last form and where do we check policy  ").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/div[2]/div/a"));
}
