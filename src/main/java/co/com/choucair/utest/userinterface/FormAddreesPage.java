package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class FormAddreesPage {

    public static final Target INPUT_CITY = Target.the("Where do write the city  ").
            located(By.id("city"));
    public static final Target SELECT_CITY = Target.the("Bring the city selected").
            located(By.className("pac-item"));
    public static final Target INPUT_ZIP = Target.the("Where do write the Postal code  ").
            located(By.id("zip"));
    public static final Target INPUT_CONTRY = Target.the("Where do we select the country").
            located(By.className("ui-select"));
    public static final Target LIST_CONTRIES = Target.the("Bring the list of countries").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/ul"));
    public static final Target SELECT_COUNTRY = Target.the("Bring the country selected").
            located(By.xpath("//*[@id=\"ui-select-choices-row-2-46\"]/span/div"));
    public static final Target NEXTDEVICES_BUTTON = Target.the("button that shows us the form of devices ").
            located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a"));
    public static final By LIST = By.cssSelector("");
}
