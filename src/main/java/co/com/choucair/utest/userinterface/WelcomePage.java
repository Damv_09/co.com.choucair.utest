package co.com.choucair.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class WelcomePage {
    public static final Target WELCOME = Target.the("Give the welcome to utest community ").
            located(By.xpath("//*[@id=\"mainContent\"]/div/div/div[1]/div/h1"));
}
