package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.ComputerBuilder;
import co.com.choucair.utest.model.ContryBuilder;
import co.com.choucair.utest.userinterface.FormAddreesPage;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedComputer implements Interaction {
    private String computer;

    public SelectedComputer(String computer) {
        this.computer = computer;
    }

    public static ComputerBuilder ofName(String computer) {
        return new ComputerBuilder().ofName(computer);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_COMPUTER),
                WaitUntil.the(FormDevicesPage.SELECT_COMPUTER.of(computer), isVisible()),
                Click.on(FormDevicesPage.SELECT_COMPUTER.of(computer))
        );
    }


}
