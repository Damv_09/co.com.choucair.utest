package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.ModelBuilder;
import co.com.choucair.utest.model.VersionBuilder;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedModel implements Interaction {
    private String model;

    public SelectedModel(String model) {
        this.model = model;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_MODEL),
                WaitUntil.the(FormDevicesPage.SELECT_MODEL.of(model), isVisible()),
                Click.on(FormDevicesPage.SELECT_MODEL.of(model))
        );
    }

    public static ModelBuilder ofName(String model) {
        return new ModelBuilder().ofName(model);
    }
}
