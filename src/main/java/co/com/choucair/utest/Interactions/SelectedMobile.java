package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.MobileBuilder;
import co.com.choucair.utest.model.VersionBuilder;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedMobile implements Interaction {
    private String mobile;

    public SelectedMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_MOBILEDEVICE),
                WaitUntil.the(FormDevicesPage.SELECT_MOBILEDEVICE.of(mobile), isVisible()),
                Click.on(FormDevicesPage.SELECT_MOBILEDEVICE.of(mobile))
        );
    }

    public static MobileBuilder ofName(String mobile) {
        return new MobileBuilder().ofName(mobile);
    }

}

