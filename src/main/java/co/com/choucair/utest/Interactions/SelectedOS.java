package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.MobileOSBuilder;
import co.com.choucair.utest.model.VersionBuilder;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedOS implements Interaction {
    private String mobileOS;

    public SelectedOS(String mobileOS) {
        this.mobileOS = mobileOS;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_OS),
                WaitUntil.the(FormDevicesPage.SELECT_OS.of(mobileOS), isVisible()),
                Click.on(FormDevicesPage.SELECT_OS.of(mobileOS))
        );
    }

    public static MobileOSBuilder ofName(String mobileOS) {
        return new MobileOSBuilder().ofName(mobileOS);
    }
}
