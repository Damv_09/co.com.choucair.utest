package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.LanguageBuilder;
import co.com.choucair.utest.model.VersionBuilder;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedLanguage implements Interaction {
    private String language;

    public SelectedLanguage(String language) {
        this.language = language;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_LANGUAGE),
                WaitUntil.the(FormDevicesPage.SELECT_LANGUAGE.of(language), isVisible()),
                Click.on(FormDevicesPage.SELECT_LANGUAGE.of(language))
        );
    }

    public static LanguageBuilder ofName(String language) {
        return new LanguageBuilder().ofName(language);
    }

}
