package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.ContryBuilder;
import co.com.choucair.utest.userinterface.FormAddreesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedContry implements Interaction {
    private String contry;

    public SelectedContry(String contry) {
        this.contry = contry;
    }

    public static ContryBuilder ofName(String contry) {
        return new ContryBuilder().ofName(contry);
    }

    @Override

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormAddreesPage.INPUT_CONTRY),
                WaitUntil.the(FormAddreesPage.SELECT_COUNTRY.of(contry), isVisible()),
                Click.on(FormAddreesPage.SELECT_COUNTRY.of(contry))
        );
    }


}
