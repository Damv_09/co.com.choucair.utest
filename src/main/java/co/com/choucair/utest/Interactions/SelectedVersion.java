package co.com.choucair.utest.Interactions;

import co.com.choucair.utest.model.ComputerBuilder;
import co.com.choucair.utest.model.VersionBuilder;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectedVersion implements Interaction {
    private String version;

    public SelectedVersion(String version) {
        this.version = version;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(FormDevicesPage.INPUT_VERSION),
                WaitUntil.the(FormDevicesPage.SELECT_VERSION.of(version), isVisible()),
                Click.on(FormDevicesPage.SELECT_VERSION.of(version))
        );
    }

    public static VersionBuilder ofName(String version) {
        return new VersionBuilder().ofName(version);
    }
}
