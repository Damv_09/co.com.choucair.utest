package co.com.choucair.utest.model;

public class DataInformation {

private String firstName;
    private String lastName;
    private String email;
    private String dateMonth;
    private String dateDay;
    private String dateYear;
    private String location;
    private String zip;
    private String contry;
    private String computer;
    private String version;
    private String language;
    private String mobileDevie;
    private String  mobilemodel;
    private String  mobileSO;
    private String password;
    private String confirmPassword;
    private String welcome;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateMonth() {
        return dateMonth;
    }

    public void setDateMonth(String dateMonth) {
        this.dateMonth = dateMonth;
    }

    public String getDateDay() {
        return dateDay;
    }

    public void setDateDay(String dateDay) {
        this.dateDay = dateDay;
    }

    public String getDateYear() {
        return dateYear;
    }

    public void setDateYear(String dateYear) {
        this.dateYear = dateYear;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getContry() {
        return contry;
    }

    public void setContry(String contry) {
        this.contry = contry;
    }

    public String getComputer() {
        return computer;
    }

    public void setComputer(String computer) {
        this.computer = computer;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMobileDevie() {
        return mobileDevie;
    }

    public void setMobileDevie(String mobileDevie) {
        this.mobileDevie = mobileDevie;
    }

    public String getMobilemodel() {
        return mobilemodel;
    }

    public void setMobilemodel(String mobilemodel) {
        this.mobilemodel = mobilemodel;
    }

    public String getMobileSO() {
        return mobileSO;
    }

    public void setMobileSO(String mobileSO) {
        this.mobileSO = mobileSO;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getWelcome() {
        return welcome;
    }

    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }
}
