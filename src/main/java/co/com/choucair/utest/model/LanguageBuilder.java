package co.com.choucair.utest.model;

import co.com.choucair.utest.Interactions.SelectedLanguage;
import co.com.choucair.utest.Interactions.SelectedVersion;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LanguageBuilder {
    private String name;

    public LanguageBuilder ofName(String name) {
        this.name = name;
        return this;
    }

    public Performable in(Target ubication) {
        return instrumented(SelectedLanguage.class, name, ubication);
    }
}
