package co.com.choucair.utest.model;

import co.com.choucair.utest.Interactions.SelectedModel;
import co.com.choucair.utest.Interactions.SelectedVersion;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ModelBuilder {
    private String name;

    public ModelBuilder ofName(String name) {
        this.name = name;
        return this;
    }

    public Performable in(Target ubication) {
        return instrumented(SelectedModel.class, name, ubication);
    }
}
