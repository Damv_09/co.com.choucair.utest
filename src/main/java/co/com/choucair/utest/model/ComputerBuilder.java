package co.com.choucair.utest.model;

import co.com.choucair.utest.Interactions.SelectedComputer;
import co.com.choucair.utest.Interactions.SelectedContry;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ComputerBuilder {
    private String name;

    public ComputerBuilder ofName(String name) {
        this.name = name;
        return this;
    }

    public Performable in(Target ubication) {
        return instrumented(SelectedComputer.class, name, ubication);
    }
}
