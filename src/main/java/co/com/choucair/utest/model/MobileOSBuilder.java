package co.com.choucair.utest.model;

import co.com.choucair.utest.Interactions.SelectedModel;
import co.com.choucair.utest.Interactions.SelectedOS;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class MobileOSBuilder {
    private String name;

    public MobileOSBuilder ofName(String name) {
        this.name = name;
        return this;
    }

    public Performable in(Target ubication) {
        return instrumented(SelectedOS.class, name, ubication);
    }
}
