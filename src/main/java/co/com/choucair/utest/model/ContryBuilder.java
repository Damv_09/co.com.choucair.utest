package co.com.choucair.utest.model;

import co.com.choucair.utest.Interactions.SelectedContry;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ContryBuilder {
    private String contry;

    public ContryBuilder(){
    }

    public ContryBuilder ofName(String contry) {
        this.contry = contry;
        return this;
    }

    public Performable in(Target ubication) {
        return instrumented(SelectedContry.class, contry, ubication);
    }
}
