package co.com.choucair.utest.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.TextContent;

public class WelcomePage implements Question<Boolean> {
    private String question;

    public WelcomePage(String question) {
        this.question = question;
    }

    public static WelcomePage toThe(String question) {
        return new WelcomePage(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String welcome = Text.of(co.com.choucair.utest.userinterface.WelcomePage.WELCOME).viewedBy(actor).asString();
        if(question.equals(welcome)){
            result = true;
        }else{result = false;}
        return result;
    }
}
