package co.com.choucair.utest.tasks;

import co.com.choucair.utest.Interactions.SelectedContry;
import co.com.choucair.utest.userinterface.FormAddreesPage;
import net.serenitybdd.screenplay.*;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.questions.SelectedVisibleTextValue;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class FormAddrees implements Task {

    private String location;
    private String zip;
    private String contry;

    public FormAddrees(String location, String zip, String contry) {
        this.location = location;
        this.zip = zip;
        this.contry = contry;
    }

    public static FormAddrees theLocationInformation(String location, String zip,String contry) {
        return Tasks.instrumented(FormAddrees.class,location,zip,contry);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(location).into(FormAddreesPage.INPUT_CITY),
                MoveMouse.to(FormAddreesPage.SELECT_CITY).andThen(Actions::doubleClick),
                Enter.theValue(zip).into(FormAddreesPage.INPUT_ZIP),
                SelectedContry.ofName(contry).in(FormAddreesPage.INPUT_CONTRY),
                Click.on(FormAddreesPage.NEXTDEVICES_BUTTON));
    }



}
