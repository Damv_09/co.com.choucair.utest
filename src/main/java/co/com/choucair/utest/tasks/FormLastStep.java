package co.com.choucair.utest.tasks;

import co.com.choucair.utest.userinterface.FormAddreesPage;
import co.com.choucair.utest.userinterface.FormLastStepPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class FormLastStep implements Task {

    private String password;
    private String confirmPassword;

    public FormLastStep(String password, String confirmPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public static FormLastStep thePasswordInformation(String password, String confirmPassword) {
        return Tasks.instrumented(FormLastStep.class,password,confirmPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Enter.theValue(password).into(FormLastStepPage.INPUT_PASSWORD),
                Enter.theValue(confirmPassword).into(FormLastStepPage.INPUT_CONFIRMPASSWORD),
                Click.on(FormLastStepPage.CHECK_ACCEPT_UTEST),
                Click.on(FormLastStepPage.CHECK_ACCEPT_PRIVACY),
                Click.on(FormLastStepPage.COMPLETE_BUTTON));
        try {
            Thread.sleep(3000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
