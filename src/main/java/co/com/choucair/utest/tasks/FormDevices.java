package co.com.choucair.utest.tasks;

import co.com.choucair.utest.Interactions.*;
import co.com.choucair.utest.userinterface.FormDevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;



public class FormDevices implements Task {

    private String computer;
    private String version;
    private String language;
    private String mobileDevie;
    private String  mobilemodel;
    private String  mobileSO;

    public FormDevices(String computer, String version, String language, String mobileDevie, String mobilemodel, String mobileSO) {
        this.computer = computer;
        this.version = version;
        this.language = language;
        this.mobileDevie = mobileDevie;
        this.mobilemodel = mobilemodel;
        this.mobileSO = mobileSO;
    }

    public static FormDevices theDevicesInformation(String computer, String version, String language, String mobileDevie, String mobilemodel, String mobileSO) {
        return Tasks.instrumented(FormDevices.class,computer,version,language,mobileDevie,mobilemodel,mobileSO);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectedComputer.ofName(computer).in(FormDevicesPage.INPUT_COMPUTER),
                SelectedVersion.ofName(version).in(FormDevicesPage.INPUT_VERSION),
                SelectedLanguage.ofName(language).in(FormDevicesPage.INPUT_LANGUAGE),
                SelectedMobile.ofName(mobileDevie).in(FormDevicesPage.INPUT_MOBILEDEVICE),
                SelectedModel.ofName(mobilemodel).in(FormDevicesPage.INPUT_MODEL),
                SelectedOS.ofName(mobileSO).in(FormDevicesPage.INPUT_OS),
                Click.on(FormDevicesPage.NEXTLASTSTEP_BUTTON)

        );

    }
}
