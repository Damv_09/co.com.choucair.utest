package co.com.choucair.utest.tasks;

import co.com.choucair.utest.userinterface.JoinTodayPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromBy;
import net.serenitybdd.screenplay.questions.SelectedValue;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class FillForm implements Task {

    private String firstName;
    private String lastName;
    private String email;
    private String dateMonth;
    private String dateDay;
    private String dateYear;

    public FillForm(String firstName, String lastName, String email, String dateMonth, String dateDay, String dateYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dateMonth = dateMonth;
        this.dateDay = dateDay;
        this.dateYear = dateYear;
    }

    public static FillForm thePersonalInformation(String firstName, String lastName, String email, String dateMonth, String dateDay, String dateYear) {
        return Tasks.instrumented(FillForm.class,firstName,lastName,email,dateMonth,dateDay,dateYear);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(JoinTodayPage.JOIN_BUTTON),
                Enter.theValue(firstName).into(JoinTodayPage.INPUT_FIRSTNAME),
                Enter.theValue(lastName).into(JoinTodayPage.INPUT_LASTNAME),
                Enter.theValue(email).into(JoinTodayPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText(dateMonth).from(JoinTodayPage.INPUT_DATEMONTH),
                SelectFromOptions.byVisibleText(dateDay).from(JoinTodayPage.INPUT_DATEDAY),
                SelectFromOptions.byVisibleText(dateYear).from(JoinTodayPage.INPUT_DATEYEAR),
                Click.on(JoinTodayPage.NEXTLOCATION_BUTTON)

        );
    }
}
