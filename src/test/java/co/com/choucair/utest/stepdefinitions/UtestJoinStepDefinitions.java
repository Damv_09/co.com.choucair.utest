package co.com.choucair.utest.stepdefinitions;

import co.com.choucair.utest.model.DataInformation;
import co.com.choucair.utest.questions.WelcomePage;
import co.com.choucair.utest.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class UtestJoinStepDefinitions {

    @Before
    public void setStage(){ OnStage.setTheStage(new OnlineCast()); }

    @Given("^than Diego wants to register to the community utest$")
    public void thanDiegoWantsToRegisterToTheCommunityUtest() {
        OnStage.theActorCalled("Diego").wasAbleTo(OpenUp.thePage());
    }

    @When("^he writes the infromation required for create a new count on the forms$")
    public void heWritesTheInfromationRequiredForCreateANewCountOnTheForms(List<DataInformation> dataInformation) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(FillForm.thePersonalInformation(dataInformation.get(0).getFirstName(),
                dataInformation.get(0).getLastName(),dataInformation.get(0).getEmail(),dataInformation.get(0).getDateMonth(),
                dataInformation.get(0).getDateDay(),dataInformation.get(0).getDateYear()));
        OnStage.theActorInTheSpotlight().attemptsTo(FormAddrees.theLocationInformation(dataInformation.get(0).getLocation(),
                dataInformation.get(0).getZip(),dataInformation.get(0).getContry()));
        OnStage.theActorInTheSpotlight().attemptsTo(FormDevices.theDevicesInformation(dataInformation.get(0).getComputer(),
                dataInformation.get(0).getVersion(),dataInformation.get(0).getLanguage(),dataInformation.get(0).getMobileDevie(),
                dataInformation.get(0).getMobilemodel(),dataInformation.get(0).getMobileSO()));
        OnStage.theActorInTheSpotlight().attemptsTo(FormLastStep.thePasswordInformation(dataInformation.get(0).getPassword(),
                dataInformation.get(0).getConfirmPassword()));
    }

    @Then("^he creates a new count ot the Utest$")
    public void heCreatesANewCountOtTheUtest(List<DataInformation> dataInformation) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(WelcomePage.toThe(dataInformation.get(0).getWelcome())));
    }

}
